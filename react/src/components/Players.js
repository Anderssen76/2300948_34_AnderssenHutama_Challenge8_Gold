const Players = [
    {
        id: "1",
        username: "naruto",
        email: "naruto@konoha.com",
        experience: "200",
        lvl: "20",
    },
    {
        id: "2",
        username: "sasuke",
        email: "sasuke@konoha.com",
        experience: "150",
        lvl: "15",
    },
    {
        id: "3",
        username: "sakura",
        email: "sakura@konoha.com",
        experience: "1",
        lvl: "1",
    },
]
export default Players;
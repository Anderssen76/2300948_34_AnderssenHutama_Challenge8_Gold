import React, { useState } from "react";
import { Button, Form } from 'react-bootstrap';
import "bootstrap/dist/css/bootstrap.min.css";
import Players from './Players';
import { v4 as uuid } from "uuid";
import { useNavigate } from 'react-router-dom';

function Add() {
    const [username, setUsername] = useState('');
    const [email, setEmail] = useState('');
    const [experience, setExperience] = useState('');
    const [lvl, setLvl] = useState('');

    let history = useNavigate();

    const handleSubmit = (e) => {
        e.preventDefault();
        const ids = uuid();
        let uniqueId = ids.slice(0, 5);

        // Email validation
        const emailPattern = /^[A-Za-z0-9._%+-]+@[A-Za-z0-9.-]+\.[A-Za-z]{2,4}$/;
        if (!emailPattern.test(email)) {
            alert('Please enter a valid email address.');
            return;
        }

        Players.push({ id: uniqueId, username, email, experience, lvl });
        history("/");
    }

    return (
        <div>
            <Form className="d-grid-gap-2" style={{ margin: "15rem" }}>
                <Form.Group className="mb-3" controlId="fromUsername">
                    <Form.Control type="text" placeholder="Enter Username" required onChange={(e) => setUsername(e.target.value)}>
                    </Form.Control>
                </Form.Group>
                <Form.Group className="mb-3" controlId="fromEmail">
                    <Form.Control type="text" placeholder="Enter Email" required onChange={(e) => setEmail(e.target.value)}>
                    </Form.Control>
                </Form.Group>
                <Form.Group className="mb-3" controlId="fromExperience">
                    <Form.Control type="number" min="0" placeholder="Enter Experience" required onChange={(e) => setExperience(e.target.value)}>
                    </Form.Control>
                </Form.Group>
                <Form.Group className="mb-3" controlId="fromLvl">
                    <Form.Control type="number" min="1" placeholder="Enter Lvl" required onChange={(e) => setLvl(e.target.value)}>
                    </Form.Control>
                </Form.Group>
            </Form>

            <Button onClick={(e) => handleSubmit(e)} type="submit">Submit</Button>
        </div>
    );
}

export default Add;

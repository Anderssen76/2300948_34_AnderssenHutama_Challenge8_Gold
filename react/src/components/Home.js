import React, { Fragment, useState } from 'react';
import { Button, Table } from 'react-bootstrap';
import "bootstrap/dist/css/bootstrap.min.css";
import Players from './Players';
import { Link } from 'react-router-dom';

function Home() {
    const [searchTerm, setSearchTerm] = useState('');
    const [filteredPlayers, setFilteredPlayers] = useState(Players);

    const handleEdit = (id, username, email, experience, lvl) => {
        localStorage.setItem('username', username)
        localStorage.setItem('email', email)
        localStorage.setItem('experience', experience)
        localStorage.setItem('lvl', lvl)
        localStorage.setItem('id', id)
    }

    const handleDelete = (id) => {
        var index = Players.findIndex(player => player.id === id);
        if (index !== -1) {
            Players.splice(index, 1);
            setFilteredPlayers(Players.filter(player =>
                player.username.toLowerCase().includes(searchTerm.toLowerCase())
            ));
        }
    }

    const handleSearch = (event) => {
        const newSearchTerm = event.target.value;
        setSearchTerm(newSearchTerm);
        setFilteredPlayers(Players.filter(player =>
            player.username.toLowerCase().includes(newSearchTerm) ||
            player.email.toLowerCase().includes(newSearchTerm) ||
            player.experience.toString().includes(newSearchTerm) ||
            player.lvl.toString().includes(newSearchTerm)
        ));
    }

    return (
        <Fragment>
            <div style={{ margin: "10rem" }}>
{/*Search bar */}
                <div class="search-bar mb-3">
                    <input
                        type="text"
                        placeholder="What do you want to find?"
                        value={searchTerm}
                        onChange={handleSearch}
                        class="form-control"
                    />
                    <div class="search-bar-append">
                        <span class="search-bar-text">
                            <i class="bi bi-search"></i>
                        </span>
                    </div>
                </div>
 {/*Table */}               
                <Table striped bordered hover size="sm">
                    <thead>
                        <tr>
                            <th>username</th>
                            <th>email</th>
                            <th>experience</th>
                            <th>lvl</th>
                            <th>Actions</th>
                        </tr>
                    </thead>
                    <tbody>
                        {
                            filteredPlayers && filteredPlayers.length > 0
                                ? filteredPlayers.map((item) => {
                                    return (
                                        <tr key={item.id}>
                                            <td>{item.username}</td>
                                            <td>{item.email}</td>
                                            <td>{item.experience}</td>
                                            <td>{item.lvl}</td>
                                            <td>
                                                <Button onClick={() => handleDelete(item.id)}>DELETE</Button>
                                                &nbsp;
                                                <Link to={'/edit'}>
                                                    <Button onClick={() => handleEdit(item.id, item.username, item.email, item.experience, item.lvl)}>EDIT</Button>
                                                </Link>
                                            </td>
                                        </tr>
                                    )
                                })
                                :
                                <tr>
                                    <td colSpan="5">No data available</td>
                                </tr>
                        }
                    </tbody>
                </Table>
                <br />
                <Link className="d-grid gap-2" to="/create">
                    <Button size="lg">CREATE</Button>
                </Link>
            </div>
        </Fragment>
    )
}

export default Home;

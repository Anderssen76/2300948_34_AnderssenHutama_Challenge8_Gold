import React, { useState, useEffect } from "react";
import { Button, Form } from 'react-bootstrap';
import "bootstrap/dist/css/bootstrap.min.css";
import Players from './Players';
import {  useNavigate } from 'react-router-dom'

function Edit() {
    const [username, setUsername] = useState('');
    const [email, setEmail] = useState('');
    const [experience, setExperience] = useState('');
    const [lvl, setLvl] = useState('');
    const [id, setId] = useState('');

    let history = useNavigate();

    var index = Players.map(function (e) {
        return e.id
    }).indexOf(id);

    const handleSubmit =(e) => {
        e.preventDefault();

        let a = Players[index];
        a.username = username;
        a.email = email;
        a.experience = experience;
        a.lvl = lvl;
       
        history("/")
    }
    
    useEffect(() => {
        setUsername(localStorage.getItem('username'))
        setEmail(localStorage.getItem('email'))
        setExperience(localStorage.getItem('experience'))
        setLvl(localStorage.getItem('lvl'))
        setId(localStorage.getItem('id'))
    },[])

    return (        
        <div>
            <Form className="d-grid-gap-2" style={{ margin: "15rem" }}>
                <Form.Group className="mb-3" controlId="fromUsername">
                    <Form.Control type="text" placeholder="Enter Username" value={username} required onChange={(e) => setUsername(e.target.value)}>
                    </Form.Control>
                </Form.Group>
                <Form.Group className="mb-3" controlId="fromEmail">
                    <Form.Control type="text" placeholder="Enter Email" value={email} required onChange={(e) => setEmail(e.target.value)}>
                    </Form.Control>
                </Form.Group>
                <Form.Group className="mb-3" controlId="fromExperience">
                    <Form.Control type="number" min="0" placeholder="Enter Experience" value={experience} required onChange={(e) => setExperience(e.target.value)}>
                    </Form.Control>
                </Form.Group>
                <Form.Group className="mb-3" controlId="fromLvl">
                    <Form.Control type="number" min="1" placeholder="Enter Lvl" value={lvl} required onChange={(e) => setLvl(e.target.value)}>
                    </Form.Control>
                </Form.Group>
                <Button onClick={(e) => handleSubmit(e)} type="submit">Update</Button>
            </Form>

        </div>
    )

}
export default Edit;